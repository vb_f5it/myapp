//
// Definitions for schema: http://www.adonix.com/WSS
//  file:/Users/Admin/Downloads/apache-cxf-3.1.8/bin/CAdxWebServiceXmlCC.wsdl.xml#types1
//
//
// Constructor for XML Schema item {http://www.adonix.com/WSS}CAdxMessage
//
function IMPL_CAdxMessage () {
  this.typeMarker = 'IMPL_CAdxMessage';
  this._message = null;
  this._type = null;
}

//
// accessor is IMPL_CAdxMessage.prototype.getMessage
// element get for message
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for message
// setter function is is IMPL_CAdxMessage.prototype.setMessage
//
function IMPL_CAdxMessage_getMessage() { return this._message;}

IMPL_CAdxMessage.prototype.getMessage = IMPL_CAdxMessage_getMessage;

function IMPL_CAdxMessage_setMessage(value) { this._message = value;}

IMPL_CAdxMessage.prototype.setMessage = IMPL_CAdxMessage_setMessage;
//
// accessor is IMPL_CAdxMessage.prototype.getType
// element get for type
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for type
// setter function is is IMPL_CAdxMessage.prototype.setType
//
function IMPL_CAdxMessage_getType() { return this._type;}

IMPL_CAdxMessage.prototype.getType = IMPL_CAdxMessage_getType;

function IMPL_CAdxMessage_setType(value) { this._type = value;}

IMPL_CAdxMessage.prototype.setType = IMPL_CAdxMessage_setType;
//
// Serialize {http://www.adonix.com/WSS}CAdxMessage
//
function IMPL_CAdxMessage_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    if (this._message == null) {
      xml = xml + '<message xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<message>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._message);
      xml = xml + '</message>';
    }
  }
  // block for local variables
  {
    if (this._type == null) {
      xml = xml + '<type xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<type>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._type);
      xml = xml + '</type>';
    }
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_CAdxMessage.prototype.serialize = IMPL_CAdxMessage_serialize;

function IMPL_CAdxMessage_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_CAdxMessage();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing message');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setMessage(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing type');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setType(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}ArrayOfCAdxMessage
//
function IMPL_ArrayOfCAdxMessage () {
  this.typeMarker = 'IMPL_ArrayOfCAdxMessage';
  this._type = null;
  this._message = null;
  this._messageArray = null;
}

//
// Serialize {http://www.adonix.com/WSS}ArrayOfCAdxMessage
//
function IMPL_ArrayOfCAdxMessage_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_ArrayOfCAdxMessage.prototype.serialize = IMPL_ArrayOfCAdxMessage_serialize;

function IMPL_ArrayOfCAdxMessage_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_ArrayOfCAdxMessage();

  var arrMsg = new Array();
  var sibling;
  var curElement;

  var type = cxfjsutils.getFirstElementChild(element);
  newobject._type = cxfjsutils.getNodeText(type);

  while (cxfjsutils.getNodeLocalName(element) != null) {

    curElement = cxfjsutils.getFirstElementChild(element);
    //console.log("name 2" + cxfjsutils.getNodeLocalName(curElement));
     if (curElement != null) {
     sibling  = cxfjsutils.getNextElementSibling(curElement);
    }

    if (sibling != null) {

      if(cxfjsutils.getNodeLocalName(element) != "messages"){
        break;
      }else {
        arrMsg.push(cxfjsutils.getNodeText(sibling));
        element = cxfjsutils.getNextElementSibling(element);
      }
    }

    if(element == null){
      break;
    }
  }


  newobject._messageArray = arrMsg;

  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}CAdxResultXml
//
function IMPL_CAdxResultXml () {
  this.typeMarker = 'IMPL_CAdxResultXml';
  this._messages = null;
  this._resultXml = null;
  this._status = 0;
  this._technicalInfos = null;
}

//
// accessor is IMPL_CAdxResultXml.prototype.getMessages
// element get for messages
// - element type is {http://www.adonix.com/WSS}ArrayOfCAdxMessage
// - required element
// - nillable
//
// element set for messages
// setter function is is IMPL_CAdxResultXml.prototype.setMessages
//
function IMPL_CAdxResultXml_getMessages() { return this._messages;}

IMPL_CAdxResultXml.prototype.getMessages = IMPL_CAdxResultXml_getMessages;

function IMPL_CAdxResultXml_setMessages(value) { this._messages = value;}

IMPL_CAdxResultXml.prototype.setMessages = IMPL_CAdxResultXml_setMessages;
//
// accessor is IMPL_CAdxResultXml.prototype.getResultXml
// element get for resultXml
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for resultXml
// setter function is is IMPL_CAdxResultXml.prototype.setResultXml
//
function IMPL_CAdxResultXml_getResultXml() { return this._resultXml;}

IMPL_CAdxResultXml.prototype.getResultXml = IMPL_CAdxResultXml_getResultXml;

function IMPL_CAdxResultXml_setResultXml(value) { this._resultXml = value;}

IMPL_CAdxResultXml.prototype.setResultXml = IMPL_CAdxResultXml_setResultXml;
//
// accessor is IMPL_CAdxResultXml.prototype.getStatus
// element get for status
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for status
// setter function is is IMPL_CAdxResultXml.prototype.setStatus
//
function IMPL_CAdxResultXml_getStatus() { return this._status;}

IMPL_CAdxResultXml.prototype.getStatus = IMPL_CAdxResultXml_getStatus;

function IMPL_CAdxResultXml_setStatus(value) { this._status = value;}

IMPL_CAdxResultXml.prototype.setStatus = IMPL_CAdxResultXml_setStatus;
//
// accessor is IMPL_CAdxResultXml.prototype.getTechnicalInfos
// element get for technicalInfos
// - element type is {http://www.adonix.com/WSS}CAdxTechnicalInfos
// - required element
// - nillable
//
// element set for technicalInfos
// setter function is is IMPL_CAdxResultXml.prototype.setTechnicalInfos
//
function IMPL_CAdxResultXml_getTechnicalInfos() { return this._technicalInfos;}

IMPL_CAdxResultXml.prototype.getTechnicalInfos = IMPL_CAdxResultXml_getTechnicalInfos;

function IMPL_CAdxResultXml_setTechnicalInfos(value) { this._technicalInfos = value;}

IMPL_CAdxResultXml.prototype.setTechnicalInfos = IMPL_CAdxResultXml_setTechnicalInfos;
//
// Serialize {http://www.adonix.com/WSS}CAdxResultXml
//
function IMPL_CAdxResultXml_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    if (this._messages == null) {
      xml = xml + '<messages xsi:nil=\'true\'/>';
    } else {
      xml = xml + this._messages.serialize(cxfjsutils, 'messages', null);
    }
  }
  // block for local variables
  {
    if (this._resultXml == null) {
      xml = xml + '<resultXml xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<resultXml>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._resultXml);
      xml = xml + '</resultXml>';
    }
  }
  // block for local variables
  {
    xml = xml + '<status>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._status);
    xml = xml + '</status>';
  }
  // block for local variables
  {
    if (this._technicalInfos == null) {
      xml = xml + '<technicalInfos xsi:nil=\'true\'/>';
    } else {
      xml = xml + this._technicalInfos.serialize(cxfjsutils, 'technicalInfos', null);
    }
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_CAdxResultXml.prototype.serialize = IMPL_CAdxResultXml_serialize;

function IMPL_CAdxResultXml_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_CAdxResultXml();
  //console.log("element " + new XMLSerializer().serializeToString(element) );
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var na = cxfjsutils.getNodeLocalName(element);

  var curElement = cxfjsutils.getFirstElementChild(element);

  var value = null;
  var name = cxfjsutils.getNodeLocalName(curElement);

  if(name == "messages"){

    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing messages');

    if(curElement) {
      if (!cxfjsutils.isElementNil(curElement)) {
        item = IMPL_ArrayOfCAdxMessage_deserialize(cxfjsutils, curElement);
        newobject.setMessages(item);
      }
    }

    if (curElement != null) {

//      console.log("Messages size " + newobject._messages._messageArray.length);

      if(newobject._messages._messageArray.length > 1) {
        for (var i = 1; i < newobject._messages._messageArray.length; i++) {
          curElement = cxfjsutils.getNextElementSibling(curElement);
        }
      }

//      console.log('curElement: ' + new XMLSerializer().serializeToString(curElement));
      var resultNode = cxfjsutils.getNextElementSibling(curElement);
  //    console.log('resultNode: ' + new XMLSerializer().serializeToString(resultNode));
      var statusNode = cxfjsutils.getNextElementSibling(resultNode);
    //  console.log('statusNode: ' + new XMLSerializer().serializeToString(statusNode));
      if (!cxfjsutils.isElementNil(resultNode)) {
        value = cxfjsutils.getNodeText(resultNode);
        item = value;
      }
      //console.log('result: ' + item);
      newobject.setResultXml(item);

      var item = null;
      /*if (resultNode != null) {
        resultNode = cxfjsutils.getNextElementSibling(resultNode);
      }*/
      cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
      cxfjsutils.trace('processing status');
      var value = null;
      if (!cxfjsutils.isElementNil(statusNode)) {
        value = cxfjsutils.getNodeText(statusNode);
        item = parseInt(value);
      }
      //console.log("status " + item);
      newobject.setStatus(item);
    }
  }
  else{
    var item = null;
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing resultXml');

  //  console.log('curElement: ' + cxfjsutils.traceElementName(curElement));
   // console.log('processing resultXml');

    var value = null;

    if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = value;
    }
    newobject.setResultXml(item);

    var item = null;
    if (curElement != null) {
      curElement = cxfjsutils.getNextElementSibling(curElement);
    }
    cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
    cxfjsutils.trace('processing status');
    var value = null;
    if (!cxfjsutils.isElementNil(curElement)) {
      value = cxfjsutils.getNodeText(curElement);
      item = parseInt(value);
    }
  //  console.log("status " + item);
    newobject.setStatus(item);
  }
  /*

   var item = null;
   if (curElement != null) {
   curElement = cxfjsutils.getNextElementSibling(curElement);
   }
   cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
   cxfjsutils.trace('processing technicalInfos');
   var value = null;
   if (!cxfjsutils.isElementNil(curElement)) {
   item = IMPL_CAdxTechnicalInfos_deserialize(cxfjsutils, curElement);
   }
   newobject.setTechnicalInfos(item);
   var item = null;
   if (curElement != null) {
   curElement = cxfjsutils.getNextElementSibling(curElement);
   }


   if(curElement)
   if (!cxfjsutils.isElementNil(curElement)) {
   item = IMPL_ArrayOfCAdxMessage_deserialize(cxfjsutils, curElement);
   }
   newobject.setMessages(item);*/

  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}ArrayOf_xsd_string
//
function IMPL_ArrayOf_xsd_string () {
  this.typeMarker = 'IMPL_ArrayOf_xsd_string';
}

//
// Serialize {http://www.adonix.com/WSS}ArrayOf_xsd_string
//
function IMPL_ArrayOf_xsd_string_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_ArrayOf_xsd_string.prototype.serialize = IMPL_ArrayOf_xsd_string_serialize;

function IMPL_ArrayOf_xsd_string_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_ArrayOf_xsd_string();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}CAdxTechnicalInfos
//
function IMPL_CAdxTechnicalInfos () {
  this.typeMarker = 'IMPL_CAdxTechnicalInfos';
  this._busy = '';
  this._changeLanguage = '';
  this._changeUserId = '';
  this._flushAdx = '';
  this._loadWebsDuration = 0.0;
  this._nbDistributionCycle = 0;
  this._poolDistribDuration = 0.0;
  this._poolEntryIdx = 0;
  this._poolExecDuration = 0.0;
  this._poolRequestDuration = 0.0;
  this._poolWaitDuration = 0.0;
  this._processReport = null;
  this._processReportSize = 0;
  this._reloadWebs = '';
  this._resumitAfterDBOpen = '';
  this._rowInDistribStack = 0;
  this._totalDuration = 0.0;
  this._traceRequest = null;
  this._traceRequestSize = 0;
}

//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getBusy
// element get for busy
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for busy
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setBusy
//
function IMPL_CAdxTechnicalInfos_getBusy() { return this._busy;}

IMPL_CAdxTechnicalInfos.prototype.getBusy = IMPL_CAdxTechnicalInfos_getBusy;

function IMPL_CAdxTechnicalInfos_setBusy(value) { this._busy = value;}

IMPL_CAdxTechnicalInfos.prototype.setBusy = IMPL_CAdxTechnicalInfos_setBusy;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getChangeLanguage
// element get for changeLanguage
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for changeLanguage
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setChangeLanguage
//
function IMPL_CAdxTechnicalInfos_getChangeLanguage() { return this._changeLanguage;}

IMPL_CAdxTechnicalInfos.prototype.getChangeLanguage = IMPL_CAdxTechnicalInfos_getChangeLanguage;

function IMPL_CAdxTechnicalInfos_setChangeLanguage(value) { this._changeLanguage = value;}

IMPL_CAdxTechnicalInfos.prototype.setChangeLanguage = IMPL_CAdxTechnicalInfos_setChangeLanguage;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getChangeUserId
// element get for changeUserId
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for changeUserId
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setChangeUserId
//
function IMPL_CAdxTechnicalInfos_getChangeUserId() { return this._changeUserId;}

IMPL_CAdxTechnicalInfos.prototype.getChangeUserId = IMPL_CAdxTechnicalInfos_getChangeUserId;

function IMPL_CAdxTechnicalInfos_setChangeUserId(value) { this._changeUserId = value;}

IMPL_CAdxTechnicalInfos.prototype.setChangeUserId = IMPL_CAdxTechnicalInfos_setChangeUserId;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getFlushAdx
// element get for flushAdx
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for flushAdx
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setFlushAdx
//
function IMPL_CAdxTechnicalInfos_getFlushAdx() { return this._flushAdx;}

IMPL_CAdxTechnicalInfos.prototype.getFlushAdx = IMPL_CAdxTechnicalInfos_getFlushAdx;

function IMPL_CAdxTechnicalInfos_setFlushAdx(value) { this._flushAdx = value;}

IMPL_CAdxTechnicalInfos.prototype.setFlushAdx = IMPL_CAdxTechnicalInfos_setFlushAdx;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getLoadWebsDuration
// element get for loadWebsDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for loadWebsDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setLoadWebsDuration
//
function IMPL_CAdxTechnicalInfos_getLoadWebsDuration() { return this._loadWebsDuration;}

IMPL_CAdxTechnicalInfos.prototype.getLoadWebsDuration = IMPL_CAdxTechnicalInfos_getLoadWebsDuration;

function IMPL_CAdxTechnicalInfos_setLoadWebsDuration(value) { this._loadWebsDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setLoadWebsDuration = IMPL_CAdxTechnicalInfos_setLoadWebsDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getNbDistributionCycle
// element get for nbDistributionCycle
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for nbDistributionCycle
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setNbDistributionCycle
//
function IMPL_CAdxTechnicalInfos_getNbDistributionCycle() { return this._nbDistributionCycle;}

IMPL_CAdxTechnicalInfos.prototype.getNbDistributionCycle = IMPL_CAdxTechnicalInfos_getNbDistributionCycle;

function IMPL_CAdxTechnicalInfos_setNbDistributionCycle(value) { this._nbDistributionCycle = value;}

IMPL_CAdxTechnicalInfos.prototype.setNbDistributionCycle = IMPL_CAdxTechnicalInfos_setNbDistributionCycle;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getPoolDistribDuration
// element get for poolDistribDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for poolDistribDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setPoolDistribDuration
//
function IMPL_CAdxTechnicalInfos_getPoolDistribDuration() { return this._poolDistribDuration;}

IMPL_CAdxTechnicalInfos.prototype.getPoolDistribDuration = IMPL_CAdxTechnicalInfos_getPoolDistribDuration;

function IMPL_CAdxTechnicalInfos_setPoolDistribDuration(value) { this._poolDistribDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setPoolDistribDuration = IMPL_CAdxTechnicalInfos_setPoolDistribDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getPoolEntryIdx
// element get for poolEntryIdx
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for poolEntryIdx
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setPoolEntryIdx
//
function IMPL_CAdxTechnicalInfos_getPoolEntryIdx() { return this._poolEntryIdx;}

IMPL_CAdxTechnicalInfos.prototype.getPoolEntryIdx = IMPL_CAdxTechnicalInfos_getPoolEntryIdx;

function IMPL_CAdxTechnicalInfos_setPoolEntryIdx(value) { this._poolEntryIdx = value;}

IMPL_CAdxTechnicalInfos.prototype.setPoolEntryIdx = IMPL_CAdxTechnicalInfos_setPoolEntryIdx;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getPoolExecDuration
// element get for poolExecDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for poolExecDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setPoolExecDuration
//
function IMPL_CAdxTechnicalInfos_getPoolExecDuration() { return this._poolExecDuration;}

IMPL_CAdxTechnicalInfos.prototype.getPoolExecDuration = IMPL_CAdxTechnicalInfos_getPoolExecDuration;

function IMPL_CAdxTechnicalInfos_setPoolExecDuration(value) { this._poolExecDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setPoolExecDuration = IMPL_CAdxTechnicalInfos_setPoolExecDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getPoolRequestDuration
// element get for poolRequestDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for poolRequestDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setPoolRequestDuration
//
function IMPL_CAdxTechnicalInfos_getPoolRequestDuration() { return this._poolRequestDuration;}

IMPL_CAdxTechnicalInfos.prototype.getPoolRequestDuration = IMPL_CAdxTechnicalInfos_getPoolRequestDuration;

function IMPL_CAdxTechnicalInfos_setPoolRequestDuration(value) { this._poolRequestDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setPoolRequestDuration = IMPL_CAdxTechnicalInfos_setPoolRequestDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getPoolWaitDuration
// element get for poolWaitDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for poolWaitDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setPoolWaitDuration
//
function IMPL_CAdxTechnicalInfos_getPoolWaitDuration() { return this._poolWaitDuration;}

IMPL_CAdxTechnicalInfos.prototype.getPoolWaitDuration = IMPL_CAdxTechnicalInfos_getPoolWaitDuration;

function IMPL_CAdxTechnicalInfos_setPoolWaitDuration(value) { this._poolWaitDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setPoolWaitDuration = IMPL_CAdxTechnicalInfos_setPoolWaitDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getProcessReport
// element get for processReport
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for processReport
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setProcessReport
//
function IMPL_CAdxTechnicalInfos_getProcessReport() { return this._processReport;}

IMPL_CAdxTechnicalInfos.prototype.getProcessReport = IMPL_CAdxTechnicalInfos_getProcessReport;

function IMPL_CAdxTechnicalInfos_setProcessReport(value) { this._processReport = value;}

IMPL_CAdxTechnicalInfos.prototype.setProcessReport = IMPL_CAdxTechnicalInfos_setProcessReport;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getProcessReportSize
// element get for processReportSize
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for processReportSize
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setProcessReportSize
//
function IMPL_CAdxTechnicalInfos_getProcessReportSize() { return this._processReportSize;}

IMPL_CAdxTechnicalInfos.prototype.getProcessReportSize = IMPL_CAdxTechnicalInfos_getProcessReportSize;

function IMPL_CAdxTechnicalInfos_setProcessReportSize(value) { this._processReportSize = value;}

IMPL_CAdxTechnicalInfos.prototype.setProcessReportSize = IMPL_CAdxTechnicalInfos_setProcessReportSize;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getReloadWebs
// element get for reloadWebs
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for reloadWebs
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setReloadWebs
//
function IMPL_CAdxTechnicalInfos_getReloadWebs() { return this._reloadWebs;}

IMPL_CAdxTechnicalInfos.prototype.getReloadWebs = IMPL_CAdxTechnicalInfos_getReloadWebs;

function IMPL_CAdxTechnicalInfos_setReloadWebs(value) { this._reloadWebs = value;}

IMPL_CAdxTechnicalInfos.prototype.setReloadWebs = IMPL_CAdxTechnicalInfos_setReloadWebs;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getResumitAfterDBOpen
// element get for resumitAfterDBOpen
// - element type is {http://www.w3.org/2001/XMLSchema}boolean
// - required element
//
// element set for resumitAfterDBOpen
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setResumitAfterDBOpen
//
function IMPL_CAdxTechnicalInfos_getResumitAfterDBOpen() { return this._resumitAfterDBOpen;}

IMPL_CAdxTechnicalInfos.prototype.getResumitAfterDBOpen = IMPL_CAdxTechnicalInfos_getResumitAfterDBOpen;

function IMPL_CAdxTechnicalInfos_setResumitAfterDBOpen(value) { this._resumitAfterDBOpen = value;}

IMPL_CAdxTechnicalInfos.prototype.setResumitAfterDBOpen = IMPL_CAdxTechnicalInfos_setResumitAfterDBOpen;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getRowInDistribStack
// element get for rowInDistribStack
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for rowInDistribStack
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setRowInDistribStack
//
function IMPL_CAdxTechnicalInfos_getRowInDistribStack() { return this._rowInDistribStack;}

IMPL_CAdxTechnicalInfos.prototype.getRowInDistribStack = IMPL_CAdxTechnicalInfos_getRowInDistribStack;

function IMPL_CAdxTechnicalInfos_setRowInDistribStack(value) { this._rowInDistribStack = value;}

IMPL_CAdxTechnicalInfos.prototype.setRowInDistribStack = IMPL_CAdxTechnicalInfos_setRowInDistribStack;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getTotalDuration
// element get for totalDuration
// - element type is {http://www.w3.org/2001/XMLSchema}double
// - required element
//
// element set for totalDuration
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setTotalDuration
//
function IMPL_CAdxTechnicalInfos_getTotalDuration() { return this._totalDuration;}

IMPL_CAdxTechnicalInfos.prototype.getTotalDuration = IMPL_CAdxTechnicalInfos_getTotalDuration;

function IMPL_CAdxTechnicalInfos_setTotalDuration(value) { this._totalDuration = value;}

IMPL_CAdxTechnicalInfos.prototype.setTotalDuration = IMPL_CAdxTechnicalInfos_setTotalDuration;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getTraceRequest
// element get for traceRequest
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for traceRequest
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setTraceRequest
//
function IMPL_CAdxTechnicalInfos_getTraceRequest() { return this._traceRequest;}

IMPL_CAdxTechnicalInfos.prototype.getTraceRequest = IMPL_CAdxTechnicalInfos_getTraceRequest;

function IMPL_CAdxTechnicalInfos_setTraceRequest(value) { this._traceRequest = value;}

IMPL_CAdxTechnicalInfos.prototype.setTraceRequest = IMPL_CAdxTechnicalInfos_setTraceRequest;
//
// accessor is IMPL_CAdxTechnicalInfos.prototype.getTraceRequestSize
// element get for traceRequestSize
// - element type is {http://www.w3.org/2001/XMLSchema}int
// - required element
//
// element set for traceRequestSize
// setter function is is IMPL_CAdxTechnicalInfos.prototype.setTraceRequestSize
//
function IMPL_CAdxTechnicalInfos_getTraceRequestSize() { return this._traceRequestSize;}

IMPL_CAdxTechnicalInfos.prototype.getTraceRequestSize = IMPL_CAdxTechnicalInfos_getTraceRequestSize;

function IMPL_CAdxTechnicalInfos_setTraceRequestSize(value) { this._traceRequestSize = value;}

IMPL_CAdxTechnicalInfos.prototype.setTraceRequestSize = IMPL_CAdxTechnicalInfos_setTraceRequestSize;
//
// Serialize {http://www.adonix.com/WSS}CAdxTechnicalInfos
//
function IMPL_CAdxTechnicalInfos_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    xml = xml + '<busy>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._busy);
    xml = xml + '</busy>';
  }
  // block for local variables
  {
    xml = xml + '<changeLanguage>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._changeLanguage);
    xml = xml + '</changeLanguage>';
  }
  // block for local variables
  {
    xml = xml + '<changeUserId>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._changeUserId);
    xml = xml + '</changeUserId>';
  }
  // block for local variables
  {
    xml = xml + '<flushAdx>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._flushAdx);
    xml = xml + '</flushAdx>';
  }
  // block for local variables
  {
    xml = xml + '<loadWebsDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._loadWebsDuration);
    xml = xml + '</loadWebsDuration>';
  }
  // block for local variables
  {
    xml = xml + '<nbDistributionCycle>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._nbDistributionCycle);
    xml = xml + '</nbDistributionCycle>';
  }
  // block for local variables
  {
    xml = xml + '<poolDistribDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._poolDistribDuration);
    xml = xml + '</poolDistribDuration>';
  }
  // block for local variables
  {
    xml = xml + '<poolEntryIdx>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._poolEntryIdx);
    xml = xml + '</poolEntryIdx>';
  }
  // block for local variables
  {
    xml = xml + '<poolExecDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._poolExecDuration);
    xml = xml + '</poolExecDuration>';
  }
  // block for local variables
  {
    xml = xml + '<poolRequestDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._poolRequestDuration);
    xml = xml + '</poolRequestDuration>';
  }
  // block for local variables
  {
    xml = xml + '<poolWaitDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._poolWaitDuration);
    xml = xml + '</poolWaitDuration>';
  }
  // block for local variables
  {
    if (this._processReport == null) {
      xml = xml + '<processReport xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<processReport>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._processReport);
      xml = xml + '</processReport>';
    }
  }
  // block for local variables
  {
    xml = xml + '<processReportSize>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._processReportSize);
    xml = xml + '</processReportSize>';
  }
  // block for local variables
  {
    xml = xml + '<reloadWebs>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._reloadWebs);
    xml = xml + '</reloadWebs>';
  }
  // block for local variables
  {
    xml = xml + '<resumitAfterDBOpen>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._resumitAfterDBOpen);
    xml = xml + '</resumitAfterDBOpen>';
  }
  // block for local variables
  {
    xml = xml + '<rowInDistribStack>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._rowInDistribStack);
    xml = xml + '</rowInDistribStack>';
  }
  // block for local variables
  {
    xml = xml + '<totalDuration>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._totalDuration);
    xml = xml + '</totalDuration>';
  }
  // block for local variables
  {
    if (this._traceRequest == null) {
      xml = xml + '<traceRequest xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<traceRequest>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._traceRequest);
      xml = xml + '</traceRequest>';
    }
  }
  // block for local variables
  {
    xml = xml + '<traceRequestSize>';
    xml = xml + cxfjsutils.escapeXmlEntities(this._traceRequestSize);
    xml = xml + '</traceRequestSize>';
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_CAdxTechnicalInfos.prototype.serialize = IMPL_CAdxTechnicalInfos_serialize;

function IMPL_CAdxTechnicalInfos_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_CAdxTechnicalInfos();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing busy');
  var value = null;
 // console.log(curElement);
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setBusy(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing changeLanguage');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setChangeLanguage(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing changeUserId');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setChangeUserId(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing flushAdx');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setFlushAdx(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing loadWebsDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setLoadWebsDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing nbDistributionCycle');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseInt(value);
  }
  newobject.setNbDistributionCycle(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolDistribDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setPoolDistribDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolEntryIdx');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseInt(value);
  }
  newobject.setPoolEntryIdx(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolExecDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setPoolExecDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolRequestDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setPoolRequestDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolWaitDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setPoolWaitDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing processReport');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setProcessReport(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing processReportSize');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseInt(value);
  }
  newobject.setProcessReportSize(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing reloadWebs');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setReloadWebs(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing resumitAfterDBOpen');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = (value == 'true');
  }
  newobject.setResumitAfterDBOpen(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing rowInDistribStack');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseInt(value);
  }
  newobject.setRowInDistribStack(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing totalDuration');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseFloat(value);
  }
  newobject.setTotalDuration(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing traceRequest');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setTraceRequest(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing traceRequestSize');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = parseInt(value);
  }
  newobject.setTraceRequestSize(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}CAdxParamKeyValue
//
function IMPL_CAdxParamKeyValue (key, value) {
  this.typeMarker = 'IMPL_CAdxParamKeyValue';
  this._key = key;
  this._value = value;
}

//
// accessor is IMPL_CAdxParamKeyValue.prototype.getKey
// element get for key
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for key
// setter function is is IMPL_CAdxParamKeyValue.prototype.setKey
//
function IMPL_CAdxParamKeyValue_getKey() { return this._key;}

IMPL_CAdxParamKeyValue.prototype.getKey = IMPL_CAdxParamKeyValue_getKey;

function IMPL_CAdxParamKeyValue_setKey(value) { this._key = value;}

IMPL_CAdxParamKeyValue.prototype.setKey = IMPL_CAdxParamKeyValue_setKey;
//
// accessor is IMPL_CAdxParamKeyValue.prototype.getValue
// element get for value
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for value
// setter function is is IMPL_CAdxParamKeyValue.prototype.setValue
//
function IMPL_CAdxParamKeyValue_getValue() { return this._value;}

IMPL_CAdxParamKeyValue.prototype.getValue = IMPL_CAdxParamKeyValue_getValue;

function IMPL_CAdxParamKeyValue_setValue(value) { this._value = value;}

IMPL_CAdxParamKeyValue.prototype.setValue = IMPL_CAdxParamKeyValue_setValue;
//
// Serialize {http://www.adonix.com/WSS}CAdxParamKeyValue
//
function IMPL_CAdxParamKeyValue_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    if (this._key == null) {
      xml = xml + '<key xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<key>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._key);
      xml = xml + '</key>';
    }
  }
  // block for local variables
  {
    if (this._value == null) {
      xml = xml + '<value xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<value>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._value);
      xml = xml + '</value>';
    }
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_CAdxParamKeyValue.prototype.serialize = IMPL_CAdxParamKeyValue_serialize;

function IMPL_CAdxParamKeyValue_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_CAdxParamKeyValue();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing key');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setKey(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing value');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setValue(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}ArrayOfCAdxParamKeyValue
//
function IMPL_ArrayOfCAdxParamKeyValue (arrKeys) {
  this.typeMarker = 'IMPL_ArrayOfCAdxParamKeyValue';
  this._keysArray = arrKeys;
}

function IMPL_CAdxParamKeyValue_getArray() { return this._keysArray;}

IMPL_CAdxParamKeyValue.prototype.getArray = IMPL_CAdxParamKeyValue_getArray;

function IMPL_CAdxParamKeyValue_setArray(value) { this._keysArray = value;}

IMPL_CAdxParamKeyValue.prototype.setArray = IMPL_CAdxParamKeyValue_setArray;

//
// Serialize {http://www.adonix.com/WSS}ArrayOfCAdxParamKeyValue
//
function IMPL_ArrayOfCAdxParamKeyValue_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    if (this._keysArray == null) {
      xml = xml + '<key xsi:nil=\'true\'/>';
    } else {
      for(var i = 0; i < this._keysArray.length; i++){
        xml = xml + this._keysArray[i].serialize(cxfjsutils, 'keys', null);
      }
    }
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_ArrayOfCAdxParamKeyValue.prototype.serialize = IMPL_ArrayOfCAdxParamKeyValue_serialize;

function IMPL_ArrayOfCAdxParamKeyValue_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_ArrayOfCAdxParamKeyValue();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  return newobject;
}

//
// Constructor for XML Schema item {http://www.adonix.com/WSS}CAdxCallContext
//
var x3user;
var x3pass;
function IMPL_CAdxCallContext (user, pass) {
  this.typeMarker = 'IMPL_CAdxCallContext';
  this._codeLang = 'POR';
  if(user && pass) {
    this._codeUser = user;
    this._password = pass;
    x3user=user;
    x3pass=pass;
  }
  this._poolAlias = 'V11';
  //this._poolAlias = 'PROTOTBQ';
  this._poolId = null;
  this._requestConfig = "adxwss.trace.off=on&adxwss.trace.size=16384&adonix.trace.on=on&adonix.trace.level=3&adonix.trace.size=8&adxwss.optreturn=JSON";
}

//
// accessor is IMPL_CAdxCallContext.prototype.getCodeLang
// element get for codeLang
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for codeLang
// setter function is is IMPL_CAdxCallContext.prototype.setCodeLang
//
function IMPL_CAdxCallContext_getCodeLang() { return this._codeLang;}

IMPL_CAdxCallContext.prototype.getCodeLang = IMPL_CAdxCallContext_getCodeLang;

function IMPL_CAdxCallContext_setCodeLang(value) { this._codeLang = value;}

IMPL_CAdxCallContext.prototype.setCodeLang = IMPL_CAdxCallContext_setCodeLang;
//
// accessor is IMPL_CAdxCallContext.prototype.getCodeUser
// element get for codeUser
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for codeUser
// setter function is is IMPL_CAdxCallContext.prototype.setCodeUser
//
function IMPL_CAdxCallContext_getCodeUser() { return this._codeUser;}

IMPL_CAdxCallContext.prototype.getCodeUser = IMPL_CAdxCallContext_getCodeUser;

function IMPL_CAdxCallContext_setCodeUser(value) { this._codeUser = value;}

IMPL_CAdxCallContext.prototype.setCodeUser = IMPL_CAdxCallContext_setCodeUser;
//
// accessor is IMPL_CAdxCallContext.prototype.getPassword
// element get for password
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for password
// setter function is is IMPL_CAdxCallContext.prototype.setPassword
//
function IMPL_CAdxCallContext_getPassword() { return this._password;}

IMPL_CAdxCallContext.prototype.getPassword = IMPL_CAdxCallContext_getPassword;

function IMPL_CAdxCallContext_setPassword(value) { this._password = value;}

IMPL_CAdxCallContext.prototype.setPassword = IMPL_CAdxCallContext_setPassword;
//
// accessor is IMPL_CAdxCallContext.prototype.getPoolAlias
// element get for poolAlias
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for poolAlias
// setter function is is IMPL_CAdxCallContext.prototype.setPoolAlias
//
function IMPL_CAdxCallContext_getPoolAlias() { return this._poolAlias;}

IMPL_CAdxCallContext.prototype.getPoolAlias = IMPL_CAdxCallContext_getPoolAlias;

function IMPL_CAdxCallContext_setPoolAlias(value) { this._poolAlias = value;}

IMPL_CAdxCallContext.prototype.setPoolAlias = IMPL_CAdxCallContext_setPoolAlias;
//
// accessor is IMPL_CAdxCallContext.prototype.getPoolId
// element get for poolId
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for poolId
// setter function is is IMPL_CAdxCallContext.prototype.setPoolId
//
function IMPL_CAdxCallContext_getPoolId() { return this._poolId;}

IMPL_CAdxCallContext.prototype.getPoolId = IMPL_CAdxCallContext_getPoolId;

function IMPL_CAdxCallContext_setPoolId(value) { this._poolId = value;}

IMPL_CAdxCallContext.prototype.setPoolId = IMPL_CAdxCallContext_setPoolId;
//
// accessor is IMPL_CAdxCallContext.prototype.getRequestConfig
// element get for requestConfig
// - element type is {http://www.w3.org/2001/XMLSchema}string
// - required element
// - nillable
//
// element set for requestConfig
// setter function is is IMPL_CAdxCallContext.prototype.setRequestConfig
//
function IMPL_CAdxCallContext_getRequestConfig() { return this._requestConfig;}

IMPL_CAdxCallContext.prototype.getRequestConfig = IMPL_CAdxCallContext_getRequestConfig;

function IMPL_CAdxCallContext_setRequestConfig(value) { this._requestConfig = value;}

IMPL_CAdxCallContext.prototype.setRequestConfig = IMPL_CAdxCallContext_setRequestConfig;
//
// Serialize {http://www.adonix.com/WSS}CAdxCallContext
//
function IMPL_CAdxCallContext_serialize(cxfjsutils, elementName, extraNamespaces) {
  var xml = '';
  if (elementName !== null) {
    xml = xml + '<';
    xml = xml + elementName;
    if (extraNamespaces) {
      xml = xml + ' ' + extraNamespaces;
    }
    xml = xml + '>';
  }
  // block for local variables
  {
    if (this._codeLang == null) {
      xml = xml + '<codeLang xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<codeLang>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._codeLang);
      xml = xml + '</codeLang>';
    }
  }
  // block for local variables
  {
    if (this._codeUser == null) {
      xml = xml + '<codeUser xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<codeUser>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._codeUser);
      xml = xml + '</codeUser>';
    }
  }
  // block for local variables
  {
    if (this._password == null) {
      xml = xml + '<password xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<password>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._password);
      xml = xml + '</password>';
    }
  }
  // block for local variables
  {
    if (this._poolAlias == null) {
      xml = xml + '<poolAlias xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<poolAlias>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._poolAlias);
      xml = xml + '</poolAlias>';
    }
  }
  // block for local variables
  {
    if (this._poolId == null) {
      xml = xml + '<poolId xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<poolId>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._poolId);
      xml = xml + '</poolId>';
    }
  }
  // block for local variables
  {
    if (this._requestConfig == null) {
      xml = xml + '<requestConfig xsi:nil=\'true\'/>';
    } else {
      xml = xml + '<requestConfig>';
      xml = xml + cxfjsutils.escapeXmlEntities(this._requestConfig);
      xml = xml + '</requestConfig>';
    }
  }
  if (elementName !== null) {
    xml = xml + '</';
    xml = xml + elementName;
    xml = xml + '>';
  }
  return xml;
}

IMPL_CAdxCallContext.prototype.serialize = IMPL_CAdxCallContext_serialize;

function IMPL_CAdxCallContext_deserialize (cxfjsutils, element) {
  var newobject = new IMPL_CAdxCallContext();
  cxfjsutils.trace('element: ' + cxfjsutils.traceElementName(element));
  var curElement = cxfjsutils.getFirstElementChild(element);
  var item;
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing codeLang');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setCodeLang(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing codeUser');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setCodeUser(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing password');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setPassword(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolAlias');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setPoolAlias(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing poolId');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setPoolId(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  cxfjsutils.trace('curElement: ' + cxfjsutils.traceElementName(curElement));
  cxfjsutils.trace('processing requestConfig');
  var value = null;
  if (!cxfjsutils.isElementNil(curElement)) {
    value = cxfjsutils.getNodeText(curElement);
    item = value;
  }
  newobject.setRequestConfig(item);
  var item = null;
  if (curElement != null) {
    curElement = cxfjsutils.getNextElementSibling(curElement);
  }
  return newobject;
}

//
// Definitions for service: {http://www.adonix.com/WSS}CAdxWebServiceXmlCCService
//

// Javascript for {http://www.adonix.com/WSS}CAdxWebServiceXmlCC

function IMPL_CAdxWebServiceXmlCC () {
  this.jsutils = new CxfApacheOrgUtil();
  this.jsutils.interfaceObject = this;
  this.synchronous = false;
  //this.url = 'http://f5it.dscloud.me:28880/adxwsvc/services/CAdxWebServiceXmlCC';
  //this.url = 'http://f5it.dscloud.me:8125/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC';
 // this.url = 'https://qa.faturaiqos.pt/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC';
  //this.url = 'http://195.22.19.62:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC';
  this.url = 'http://f5it.dscloud.me:8126/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC';
  this.client = null;
  this.response = null;
  this.globalElementSerializers = [];
  this.globalElementDeserializers = [];
  this.globalElementSerializers['{http://www.adonix.com/WSS}CAdxMessage'] = IMPL_CAdxMessage_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}CAdxMessage'] = IMPL_CAdxMessage_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}ArrayOfCAdxMessage'] = IMPL_ArrayOfCAdxMessage_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}ArrayOfCAdxMessage'] = IMPL_ArrayOfCAdxMessage_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}CAdxResultXml'] = IMPL_CAdxResultXml_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}CAdxResultXml'] = IMPL_CAdxResultXml_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}ArrayOf_xsd_string'] = IMPL_ArrayOf_xsd_string_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}ArrayOf_xsd_string'] = IMPL_ArrayOf_xsd_string_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}CAdxTechnicalInfos'] = IMPL_CAdxTechnicalInfos_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}CAdxTechnicalInfos'] = IMPL_CAdxTechnicalInfos_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}CAdxParamKeyValue'] = IMPL_CAdxParamKeyValue_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}CAdxParamKeyValue'] = IMPL_CAdxParamKeyValue_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}ArrayOfCAdxParamKeyValue'] = IMPL_ArrayOfCAdxParamKeyValue_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}ArrayOfCAdxParamKeyValue'] = IMPL_ArrayOfCAdxParamKeyValue_deserialize;
  this.globalElementSerializers['{http://www.adonix.com/WSS}CAdxCallContext'] = IMPL_CAdxCallContext_serialize;
  this.globalElementDeserializers['{http://www.adonix.com/WSS}CAdxCallContext'] = IMPL_CAdxCallContext_deserialize;
}

function IMPL_getDataXmlSchema_op_onsuccess(client, responseXml) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_getDataXmlSchemaResponse_deserializeResponse');
    responseObject = IMPL_getDataXmlSchemaResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.getDataXmlSchema_onsuccess = IMPL_getDataXmlSchema_op_onsuccess;

function IMPL_getDataXmlSchema_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.getDataXmlSchema_onerror = IMPL_getDataXmlSchema_op_onerror;

//
// Operation {http://www.adonix.com/WSS}getDataXmlSchema
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_getDataXmlSchema_op(successCallback, errorCallback, callContext, publicName) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(2);
  args[0] = callContext;
  args[1] = publicName;
  xml = this.getDataXmlSchemaRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { closureThis.getDataXmlSchema_onsuccess(client, responseXml); };
  this.client.onerror = function(client) { closureThis.getDataXmlSchema_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

IMPL_CAdxWebServiceXmlCC.prototype.getDataXmlSchema = IMPL_getDataXmlSchema_op;

function IMPL_getDataXmlSchemaRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:getDataXmlSchema>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  xml = xml + '</jns0:getDataXmlSchema>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.getDataXmlSchemaRequest_serializeInput = IMPL_getDataXmlSchemaRequest_serializeInput;

function IMPL_getDataXmlSchemaResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_actionObject_op_onsuccess(client, responseXml) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_actionObjectResponse_deserializeResponse');
    responseObject = IMPL_actionObjectResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObject_onsuccess = IMPL_actionObject_op_onsuccess;

function IMPL_actionObject_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObject_onerror = IMPL_actionObject_op_onerror;

//
// Operation {http://www.adonix.com/WSS}actionObject
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_actionObject_op(successCallback, errorCallback, callContext, publicName, actionCode, objectXml) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(4);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = actionCode;
  args[3] = objectXml;
  xml = this.actionObjectRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { closureThis.actionObject_onsuccess(client, responseXml); };
  this.client.onerror = function(client) { closureThis.actionObject_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObject = IMPL_actionObject_op;

function IMPL_actionObjectRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:actionObject>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + '<actionCode>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[2]);
    xml = xml + '</actionCode>';
  }
  // block for local variables
  {
    xml = xml + '<objectXml>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[3]);
    xml = xml + '</objectXml>';
  }
  xml = xml + '</jns0:actionObject>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObjectRequest_serializeInput = IMPL_actionObjectRequest_serializeInput;

function IMPL_actionObjectResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_modify_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    //alert(new XMLSerializer().serializeToString(element));
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_modifyResponse_deserializeResponse');
    responseObject = IMPL_modifyResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.modify_onsuccess = IMPL_modify_op_onsuccess;

function IMPL_modify_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.modify_onerror = IMPL_modify_op_onerror;

//
// Operation {http://www.adonix.com/WSS}modify
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_modify_op(successCallback, errorCallback, callContext, publicName, objectKeys, objectXml, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(4);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  args[3] = objectXml;
  xml = IMPL_modifyRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_modify_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.modify_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
 // console.log("user " + x3user + " pass " + x3pass);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.modify = IMPL_modify_op;

function IMPL_modifyRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:modify>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  // block for local variables
  {
    xml = xml + '<objectXml>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[3]);
    xml = xml + '</objectXml>';
  }
  xml = xml + '</jns0:modify>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.modifyRequest_serializeInput = IMPL_modifyRequest_serializeInput;

function IMPL_modifyResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_save_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    console.log(responseXml);
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_saveResponse_deserializeResponse');
    responseObject = IMPL_saveResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.save_onsuccess = IMPL_save_op_onsuccess;

function IMPL_save_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.save_onerror = IMPL_save_op_onerror;

//
// Operation {http://www.adonix.com/WSS}save
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_save_op(successCallback, errorCallback, callContext, publicName, objectXml, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(3);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectXml;
  xml = IMPL_saveRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_save_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.save_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);

 // console.log("user " + x3user + " pass " + x3pass);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.save = IMPL_save_op;

function IMPL_saveRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:save>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + '<objectXml>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[2]);
    xml = xml + '</objectXml>';
  }
  xml = xml + '</jns0:save>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.saveRequest_serializeInput = IMPL_saveRequest_serializeInput;

function IMPL_saveResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_run_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    console.log(responseXml);
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_runResponse_deserializeResponse');
    responseObject = IMPL_runResponse_deserializeResponse(this.jsutils, element);
    console.log(responseObject);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.run_onsuccess = IMPL_run_op_onsuccess;

function IMPL_run_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.run_onerror = IMPL_run_op_onerror;

//
// Operation {http://www.adonix.com/WSS}run
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_run_op(successCallback, errorCallback, callContext, publicName, inputXml, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(3);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = inputXml;
  xml = IMPL_runRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_run_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.run_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  console.log(callContext);
 // console.log("user " + x3user + " pass " + x3pass);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.run = IMPL_run_op;

function IMPL_runRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:run>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + '<inputXml>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[2]);
    xml = xml + '</inputXml>';
  }
  xml = xml + '</jns0:run>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.runRequest_serializeInput = IMPL_runRequest_serializeInput;

function IMPL_runResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_delete_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_deleteResponse_deserializeResponse');
    responseObject = IMPL_deleteResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.delete_onsuccess = IMPL_delete_op_onsuccess;

function IMPL_delete_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.delete_onerror = IMPL_delete_op_onerror;

//
// Operation {http://www.adonix.com/WSS}delete
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
//
function IMPL_delete_op(successCallback, errorCallback, callContext, publicName, objectKeys, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(3);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  xml = IMPL_deleteRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_delete_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.delete_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.delete = IMPL_delete_op;

function IMPL_deleteRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:delete>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  xml = xml + '</jns0:delete>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.deleteRequest_serializeInput = IMPL_deleteRequest_serializeInput;

function IMPL_deleteResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_getDescription_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_getDescriptionResponse_deserializeResponse');
    responseObject = IMPL_getDescriptionResponse_deserializeResponse(this.jsutils, element);
    console.log(responseObject);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.getDescription_onsuccess = IMPL_getDescription_op_onsuccess;

function IMPL_getDescription_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.getDescription_onerror = IMPL_getDescription_op_onerror;

//
// Operation {http://www.adonix.com/WSS}getDescription
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_getDescription_op(successCallback, errorCallback, callContext, publicName, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(2);
  args[0] = callContext;
  args[1] = publicName;
  xml = IMPL_getDescriptionRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_getDescription_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.getDescription_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.getDescription = IMPL_getDescription_op;

function IMPL_getDescriptionRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:getDescription>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  xml = xml + '</jns0:getDescription>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.getDescriptionRequest_serializeInput = IMPL_getDescriptionRequest_serializeInput;

function IMPL_getDescriptionResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_insertLines_op_onsuccess(client, responseXml) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_insertLinesResponse_deserializeResponse');
    responseObject = IMPL_insertLinesResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.insertLines_onsuccess = IMPL_insertLines_op_onsuccess;

function IMPL_insertLines_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.insertLines_onerror = IMPL_insertLines_op_onerror;

//
// Operation {http://www.adonix.com/WSS}insertLines
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
//
function IMPL_insertLines_op(successCallback, errorCallback, callContext, publicName, objectKeys, blocKey, lineKey, lineXml) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(6);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  args[3] = blocKey;
  args[4] = lineKey;
  args[5] = lineXml;
  xml = this.insertLinesRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { closureThis.insertLines_onsuccess(client, responseXml); };
  this.client.onerror = function(client) { closureThis.insertLines_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

IMPL_CAdxWebServiceXmlCC.prototype.insertLines = IMPL_insertLines_op;

function IMPL_insertLinesRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:insertLines>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  // block for local variables
  {
    xml = xml + '<blocKey>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[3]);
    xml = xml + '</blocKey>';
  }
  // block for local variables
  {
    xml = xml + '<lineKey>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[4]);
    xml = xml + '</lineKey>';
  }
  // block for local variables
  {
    xml = xml + '<lineXml>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[5]);
    xml = xml + '</lineXml>';
  }
  xml = xml + '</jns0:insertLines>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.insertLinesRequest_serializeInput = IMPL_insertLinesRequest_serializeInput;

function IMPL_insertLinesResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_read_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    var name = this.jsutils.getNodeLocalName(element);
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);

      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_readResponse_deserializeResponse');

    responseObject = IMPL_readResponse_deserializeResponse(this.jsutils, element);

    console.log('calling IMPL_readResponse_deserializeResponse');
    console.log(responseObject);
    client.user_onsuccess(responseObject);

    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.read_onsuccess = IMPL_read_op_onsuccess;

function IMPL_read_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.read_onerror = IMPL_read_op_onerror;

//
// Operation {http://www.adonix.com/WSS}read
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
//
function IMPL_read_op(successCallback, errorCallback, callContext, publicName, objectKeys, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(3);
  /*  console.log(callContext);
   console.log(publicName);
   console.log(objectKeys);*/
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  xml = IMPL_readRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;

  this.client.onsuccess = function(client, responseXml) { console.log("responseXml " + new XMLSerializer().serializeToString(responseXml) );
    IMPL_read_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { closureThis.read_onerror(client); };
  var method = "read";
  var ns = "http://www.adonix.com/WSS";
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);

  //console.log("user " + x3user + " pass " + x3pass);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.read = IMPL_read_op;

function IMPL_readRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:read>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  xml = xml + '</jns0:read>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.readRequest_serializeInput = IMPL_readRequest_serializeInput;

function IMPL_readResponse_deserializeResponse(cxfjsutils, partElement) {

  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_deleteLines_op_onsuccess(client, responseXml) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_deleteLinesResponse_deserializeResponse');
    responseObject = IMPL_deleteLinesResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.deleteLines_onsuccess = IMPL_deleteLines_op_onsuccess;

function IMPL_deleteLines_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.deleteLines_onerror = IMPL_deleteLines_op_onerror;

//
// Operation {http://www.adonix.com/WSS}deleteLines
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
//
function IMPL_deleteLines_op(successCallback, errorCallback, callContext, publicName, objectKeys, blocKey, lineKeys) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(5);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  args[3] = blocKey;
  args[4] = lineKeys;
  xml = this.deleteLinesRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { closureThis.deleteLines_onsuccess(client, responseXml); };
  this.client.onerror = function(client) { closureThis.deleteLines_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

IMPL_CAdxWebServiceXmlCC.prototype.deleteLines = IMPL_deleteLines_op;

function IMPL_deleteLinesRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:deleteLines>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  // block for local variables
  {
    xml = xml + '<blocKey>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[3]);
    xml = xml + '</blocKey>';
  }
  // block for local variables
  {
    xml = xml + args[4].serialize(cxfjsutils, 'lineKeys', null);
  }
  xml = xml + '</jns0:deleteLines>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.deleteLinesRequest_serializeInput = IMPL_deleteLinesRequest_serializeInput;

function IMPL_deleteLinesResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_actionObjectKeys_op_onsuccess(client, responseXml) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_actionObjectKeysResponse_deserializeResponse');
    responseObject = IMPL_actionObjectKeysResponse_deserializeResponse(this.jsutils, element);
    client.user_onsuccess(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObjectKeys_onsuccess = IMPL_actionObjectKeys_op_onsuccess;

function IMPL_actionObjectKeys_op_onerror(client) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObjectKeys_onerror = IMPL_actionObjectKeys_op_onerror;

//
// Operation {http://www.adonix.com/WSS}actionObjectKeys
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
//
function IMPL_actionObjectKeys_op(successCallback, errorCallback, callContext, publicName, actionCode, objectKeys) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(4);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = actionCode;
  args[3] = objectKeys;
  xml = this.actionObjectKeysRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { closureThis.actionObjectKeys_onsuccess(client, responseXml); };
  this.client.onerror = function(client) { closureThis.actionObjectKeys_onerror(client); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders);
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObjectKeys = IMPL_actionObjectKeys_op;

function IMPL_actionObjectKeysRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:actionObjectKeys>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + '<actionCode>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[2]);
    xml = xml + '</actionCode>';
  }
  // block for local variables
  {
    xml = xml + args[3].serialize(cxfjsutils, 'objectKeys', null);
  }
  xml = xml + '</jns0:actionObjectKeys>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.actionObjectKeysRequest_serializeInput = IMPL_actionObjectKeysRequest_serializeInput;

function IMPL_actionObjectKeysResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_query_op_onsuccess(client, responseXml, deferred) {
  if (client.user_onsuccess) {
    var responseObject = null;
    var element = responseXml.documentElement;
    console.log(responseXml);
    this.jsutils.trace('responseXml: ' + this.jsutils.traceElementName(element));
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('first element child: ' + this.jsutils.traceElementName(element));
    while (!this.jsutils.isNodeNamedNS(element, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body')) {
      element = this.jsutils.getNextElementSibling(element);
      if (element == null) {
        throw 'No env:Body in message.'
      }
    }
    element = this.jsutils.getFirstElementChild(element);
    this.jsutils.trace('part element: ' + this.jsutils.traceElementName(element));
    this.jsutils.trace('calling IMPL_queryResponse_deserializeResponse');

    responseObject = IMPL_queryResponse_deserializeResponse(this.jsutils, element);
    console.log("responseObject " + responseObject);
    client.user_onsuccess(responseObject);
    deferred.resolve(responseObject);
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.query_onsuccess = IMPL_query_op_onsuccess;

function IMPL_query_op_onerror(client, deferred) {
  if (client.user_onerror) {
    var httpStatus;
    var httpStatusText;
    try {
      httpStatus = client.req.status;
      httpStatusText = client.req.statusText;
    } catch(e) {
      httpStatus = -1;
      httpStatusText = 'Error opening connection to server';
    }
    if (client.parseErrorDetails) {
      client.user_onerror(httpStatus, httpStatusText, client.parseErrorDetails(this));
    } else {
      client.user_onerror(httpStatus, httpStatusText);
    }
    deferred.reject('Credenciais erradas.');
  }
}

IMPL_CAdxWebServiceXmlCC.prototype.query_onerror = IMPL_query_op_onerror;

//
// Operation {http://www.adonix.com/WSS}query
// - bare operation. Parameters:
// - IMPL_CAdxCallContext
// - type {http://www.w3.org/2001/XMLSchema}string
// - empty element?
// - type {http://www.w3.org/2001/XMLSchema}int
//
function IMPL_query_op(successCallback, errorCallback, callContext, publicName, objectKeys, listSize, deferred) {
  this.client = new CxfApacheOrgClient(this.jsutils);
  var xml = null;
  var args = new Array(4);
  args[0] = callContext;
  args[1] = publicName;
  args[2] = objectKeys;
  args[3] = listSize;
  xml = IMPL_queryRequest_serializeInput(this.jsutils, args);
  this.client.user_onsuccess = successCallback;
  this.client.user_onerror = errorCallback;
  var closureThis = this;
  this.client.onsuccess = function(client, responseXml) { IMPL_query_op_onsuccess(client, responseXml, deferred); };
  this.client.onerror = function(client) { IMPL_query_op_onerror(client, deferred); };
  var requestHeaders = [];
  requestHeaders['SOAPAction'] = '';
  this.jsutils.trace('synchronous = ' + this.synchronous);
  var authorizationBasic = convertAccessToBasicAuth(x3user, x3pass);
  this.client.request(this.url, xml, null, this.synchronous, requestHeaders, authorizationBasic);
}

IMPL_CAdxWebServiceXmlCC.prototype.query = IMPL_query_op;

function IMPL_queryRequest_serializeInput(cxfjsutils, args) {
  var xml;
  xml = cxfjsutils.beginSoap11Message("xmlns:jns0='http://www.adonix.com/WSS' ");
  xml = xml + '<jns0:query>';
  // block for local variables
  {
    xml = xml + args[0].serialize(cxfjsutils, 'callContext', null);
  }
  // block for local variables
  {
    xml = xml + '<publicName>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[1]);
    xml = xml + '</publicName>';
  }
  // block for local variables
  {
    xml = xml + args[2].serialize(cxfjsutils, 'objectKeys', null);
  }
  // block for local variables
  {
    xml = xml + '<listSize>';
    xml = xml + cxfjsutils.escapeXmlEntities(args[3]);
    xml = xml + '</listSize>';
  }
  xml = xml + '</jns0:query>';
  xml = xml + cxfjsutils.endSoap11Message();
  return xml;
}

IMPL_CAdxWebServiceXmlCC.prototype.queryRequest_serializeInput = IMPL_queryRequest_serializeInput;

function IMPL_queryResponse_deserializeResponse(cxfjsutils, partElement) {
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  partElement = cxfjsutils.getFirstElementChild(partElement);
  cxfjsutils.trace('rpc element: ' + cxfjsutils.traceElementName(partElement));
  var returnObject = IMPL_CAdxResultXml_deserialize (cxfjsutils, partElement);

  return returnObject;
}
function IMPL_CAdxWebServiceXmlCC_IMPL_CAdxWebServiceXmlCC () {
  this.url = 'http://f5it.dscloud.me:28880/adxwsvc/services/CAdxWebServiceXmlCC';
}
IMPL_CAdxWebServiceXmlCC_IMPL_CAdxWebServiceXmlCC.prototype = new IMPL_CAdxWebServiceXmlCC;


function convertAccessToBasicAuth(user, pass) {
  var authBasic;
  if (window.btoa) {
    authBasic = window.btoa(user + ":" + pass);
    //console.log(authBasic);
  } else { //for <= IE9
    var Base64 = {
      _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
          n = e.charCodeAt(f++);
          r = e.charCodeAt(f++);
          i = e.charCodeAt(f++);
          s = n >> 2;
          o = (n & 3) << 4 | r >> 4;
          u = (r & 15) << 2 | i >> 6;
          a = i & 63;
          if (isNaN(r)) {
            u = a = 64
          } else if (isNaN(i)) {
            a = 64
          }
          t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
      }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
          s = this._keyStr.indexOf(e.charAt(f++));
          o = this._keyStr.indexOf(e.charAt(f++));
          u = this._keyStr.indexOf(e.charAt(f++));
          a = this._keyStr.indexOf(e.charAt(f++));
          n = s << 2 | o >> 4;
          r = (o & 15) << 4 | u >> 2;
          i = (u & 3) << 6 | a;
          t = t + String.fromCharCode(n);
          if (u != 64) {
            t = t + String.fromCharCode(r)
          }
          if (a != 64) {
            t = t + String.fromCharCode(i)
          }
        }
        t = Base64._utf8_decode(t);
        return t
      }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
          var r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r)
          } else if (r > 127 && r < 2048) {
            t += String.fromCharCode(r >> 6 | 192);
            t += String.fromCharCode(r & 63 | 128)
          } else {
            t += String.fromCharCode(r >> 12 | 224);
            t += String.fromCharCode(r >> 6 & 63 | 128);
            t += String.fromCharCode(r & 63 | 128)
          }
        }
        return t
      }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
          r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r);
            n++
          } else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);
            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
            n += 2
          } else {
            c2 = e.charCodeAt(n + 1);
            c3 = e.charCodeAt(n + 2);
            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
            n += 3
          }
        }
        return t
      }
    }
    // Encode the String
    var encodedString = Base64.encode(user + ":" + pass);
    if (typeof console == "undefined") {
      this.console = {
        log: function () {
        }
      };
    }
    //console.log(encodedString);

    authBasic = encodedString;
  }
  return authBasic;
}
