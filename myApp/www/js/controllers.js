angular.module('starter.controllers', [])

.controller('TransCtrl', function($scope, $translate) {

  $scope.active = 'pt';

  $scope.setActive = function(type) {
    $scope.active = type;
  };
  $scope.isActive = function(type) {
    return type === $scope.active;
  };

  $scope.changeLanguage = function (key) {
    $translate.use(key);
  };
})

.controller('SignInCtrl', function($scope, LoginService, $ionicPopup, $state,
  $ionicLoading, $translate, ShareDataService, CAdxCallContextData) {
    $scope.signIn = function(user) {

      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }

      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        duration: 10000
      });

      if(window.Connection){

        if(navigator.connection.type == Connection.NONE) {

          $ionicLoading.hide();

          $translate(['NO_NET', 'TRY']).then(function(translations) {
            $ionicPopup.alert({
              title: translations.NO_NET,
              template: translations.TRY,
              cssClass: 'custPop'
            });
          });

        } else {

          if(user != null){
            LoginService.loginUser(user.username, user.password).success(function(data) {
              $ionicLoading.hide();
              try{
                if (data._status != 1) {
                  treatWSmessages(data, $ionicPopup);
                }
                else {
                  ShareDataService.set(data);
                  CAdxCallContextData.set(new IMPL_CAdxCallContext(user.username, user.password));

                  $state.go('tab.dash');
                }
              }catch (err) {

                $translate(['ERR']).then(function(translations) {
                  $ionicPopup.alert({
                    title: translations.ERR,
                    template: err.message,
                    cssClass: 'custPop'
                  });
                });

              }
            }
          ).error(function(data) {
            $ionicLoading.hide();

            $translate(['SESSION_NOK', 'SESSION_NOK_MSG']).then(function(translations) {
              $ionicPopup.alert({
                title: translations.SESSION_NOK,
                template: translations.SESSION_NOK_MSG,
                cssClass: 'custPop'
              });
            });
          });
        }
        else{
          $ionicLoading.hide();
          $translate(['SESSION_NOK', 'SESSION_NOK_MSG']).then(function(translations) {
            $ionicPopup.alert({
              title: translations.SESSION_NOK,
              template: translations.SESSION_NOK_MSG,
              cssClass: 'custPop'
            });
          });
        }
      }

    }
  }
})

.controller('LogoutCtrl', function ($scope, $state, $timeout, $ionicHistory) {
  $scope.logout = function() {
    $scope.$root.showAvatar = false;

    $state.go('signin');

    $timeout(function () {
      $ionicHistory.clearCache();
    }, 200)
  }
})

.controller('DashCtrl', function($scope) {

  $scope.$root.showAvatar = true;

})


.controller('ChatsCtrl', function($scope, Chats) {
// With the new view caching in Ionic, Controllers are only called
// when they are recreated or on app start, instead of every page change.
// To listen for when this page is active (for example, to refresh data),
// listen for the $ionicView.enter event:
//
//$scope.$on('$ionicView.enter', function(e) {
//});

$scope.chats = Chats.all();
$scope.remove = function(chat) {
  Chats.remove(chat);
};
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
$scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
$scope.settings = {
  enableFriends: true
};
});

function treatWSmessages(data, $ionicPopup) {
  var msg = "";
  if (data != null && data._messages != null && data._messages._messageArray != null) {
    for (var i = 0; i < data._messages._messageArray.length; i++) {
      msg = msg + data._messages._messageArray[i] + "\n";
    }
  }

  if(data._status == 1){
    var alertPopup = $ionicPopup.alert({
      title: 'Alerta X3:',
      template: msg,
      cssClass: 'custPop'
    });
  }
  else{
    var alertPopup = $ionicPopup.alert({
      title: 'Erro X3:',
      template: msg,
      cssClass: 'custPop'
    });
  }
}
