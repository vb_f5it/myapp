/**
 * Created by Admin on 22/03/17.
 */

var translations = {
  "en": {
    TAB_HOME:"Home",
    USER:"User",
    PASS:"Password",
    SESSION:"Sign In",
    SESSION_NOK:"Sign In failed!",
    SESSION_NOK_MSG:"Please check your credentials!",
    NO_NET:'No internet connection!',
    TRY:'Please check the connection and try again.',
    ERR: 'App Error',
    ALERT_X3:'X3 Alert:',
    ERR_X3:'X3 Error:',
    "h2":"Today's Deals!",
    "hp1": "This page will show products on sale.",
    "hp2": "More info at: ",
    "ht": "Welcome"
  },
  "pt": {
    TAB_HOME:"Início",
    USER:"Utilizador",
    PASS:"Senha",
    SESSION:"Iniciar Sessão",
    SESSION_NOK:"Início de sessão  falhou!",
    SESSION_NOK_MSG:"Por favor verifique as suas credenciais!",
    NO_NET:'Sem ligação à Internet!',
    ERR:'Erro App',
    TRY:'Por favor verifique a ligação e tente novamente.',
    ALERT_X3:'Alerta X3:',
    ERR_X3:'Erro X3:',
    "h2":"Promoções",
    "hp1": "Esta página vai conter artigos em promoção.",
    "hp2": "Mais informação em: ",
  }
}
